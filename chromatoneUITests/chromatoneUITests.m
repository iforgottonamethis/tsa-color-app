//
//  chromatoneUITests.m
//  chromatoneUITests
//
//  Created by Umar on 11/17/15.
//  Copyright © 2015 Shazia Haroon. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface chromatoneUITests : XCTestCase

@end

@implementation chromatoneUITests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // Use recording to get started writing UI tests.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
    
    
    
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    XCUIElement *element5 = [[[app.otherElements containingType:XCUIElementTypeButton identifier:@"share"] childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:1];
    XCUIElement *element = [[element5 childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:0];
    [element tap];
    [element tap];
    [element tap];
    
    XCUIElement *element2 = [[element5 childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:2];
    [element2 tap];
    
    XCUIElement *element3 = [[element5 childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:4];
    [element3 twoFingerTap];
    
    XCUIElement *element4 = [[element5 childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:3];
    [element4 tap];
    [element4 tap];
    [element3 tap];
    [[[element5 childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:8] tap];
    
    XCUIElement *element6 = [[element5 childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:5];
    [element6 swipeDown];
    
    XCUIElement *element7 = [[element5 childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:7];
    [element7 tap];
    [element3 tap];
    [element3 tap];
    [element3 tap];
    [element7 tap];
    [element6 tap];
    [element6 tap];
    [element3 swipeLeft];
    [element4 tap];
    [element4 swipeLeft];
    [element4 tap];
    [element2 twoFingerTap];
    [element3 tap];
    [element4 tap];
    
    XCUIElement *element8 = [[element5 childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:6];
    [element8 swipeUp];
    [element3 tap];
    [element7 tap];
    [element3 tap];
    [element3 twoFingerTap];
    [element8 tap];
    [element8 tap];
    [[[[[element2 childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:4] childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:1] tap];
    [app.sliders[@"52%"] tap];
    [app.sliders[@"22%"] tap];
    [app.staticTexts[@"217"] tap];
    
    XCUIElement *key = app.keys[@"5"];
    [key tap];
    
    XCUIElementQuery *collectionViewsQuery = app.alerts[@"Green"].collectionViews;
    XCUIElement *numberFrom0255TextField = collectionViewsQuery.textFields[@"Number from 0-255"];
    [numberFrom0255TextField typeText:@"5"];
    [key tap];
    [numberFrom0255TextField typeText:@"5"];
    [collectionViewsQuery.buttons[@"OK"] tap];
    
    XCUIElement *element9 = [[[element2 childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:3] childrenMatchingType:XCUIElementTypeOther].element;
    [element9 tap];
    [element9 tap];
    [app.staticTexts[@"red: 75 green: 55 blue: 75   hex: 4B374B"] pressForDuration:0.5];
    [element9 tap];
    
    XCUIElement *randomButton = app.buttons[@"random"];
    [randomButton tap];
    [element9 swipeUp];
    [randomButton tap];
    [element9 tap];
    [element9 swipeUp];
    [element7 tap];
    
    
    
}

@end
