//
//  UHColor.m
//  CoolorsClone
//
//  Created by Umar on 10/3/15.
//  Copyright © 2015 Umar. All rights reserved.
//

#import "UHColor.h"

@implementation UHColor
@synthesize items;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)displayColorView:(UHColor  *)view withColor:(UIColor *)color
{
    
    [UIView animateWithDuration:.225 delay:0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        view.backgroundColor = color;
    }
                     completion:nil];
}




#pragma mark - Networking

- (void)getRandomColorAndHexForColorView:(UHColor  *)view
{
    NSString *urlString = @"http://www.colourlovers.com/api/colors/random&format=json";
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];

    __weak typeof(self) weakSelf = self;
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if (connectionError) {
                                   NSLog(@"Connection error: %@", connectionError.description);
                               } else {
                                   NSArray *array = [NSJSONSerialization JSONObjectWithData:data
                                                                                    options:NSJSONReadingAllowFragments
                                                                                      error:nil];
                                   NSDictionary *colorDictionary = array.firstObject;


                                   NSDictionary *RGB = colorDictionary[@"rgb"];
                                   UIColor *randomColor = [UIColor colorWithRed:[[RGB objectForKey:@"red"] floatValue]/255.0
                                                                          green:[[RGB objectForKey:@"green"] floatValue]/255.0
                                                                           blue:[[RGB objectForKey:@"blue"] floatValue]/255.0
                                                                          alpha:1.0];
                                   [weakSelf displayColorView:view withColor:randomColor];

                                   view.redColor = [RGB objectForKey:@"red"];
                                   view.blueColor = [RGB objectForKey:@"blue"];
                                   view.greenColor = [RGB objectForKey:@"green"];
                                   view.hexString = colorDictionary[@"hex"];
                                   items = [[NSMutableArray alloc] initWithObjects:@"red",@"green",@"blue", nil];


                               }
                           }];
}

-(void)setRandomColorForView:(UHColor *)view
{
    if (!view) {
        NSLog(@"THIS IS NOT GOOD");
    }
    int red = arc4random() %255;
    int green = arc4random() %255;
    int blue = arc4random() %255;
    
    view.redColor = [NSString stringWithFormat:@"%i", red];
    view.greenColor = [NSString stringWithFormat:@"%i", green];
    view.blueColor = [NSString stringWithFormat:@"%i", blue];
    view.hexString = [NSString stringWithFormat:@"%X%X%X",red,green,blue];
    
    [self displayColorView:view withColor:[UIColor colorWithRed:[[NSNumber numberWithInt:red] floatValue] / 255.0 green:[[NSNumber numberWithInt:green]floatValue] / 255.0 blue:[[NSNumber numberWithInt:blue]floatValue] / 255.0 alpha:1.0]];
}

@end
