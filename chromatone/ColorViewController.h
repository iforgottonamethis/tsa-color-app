//
//  ViewController.h
//  CoolorsClone
//
//  Created by Umar on 9/28/15.
//  Copyright © 2015 Umar. All rights reserved.
//




#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "UHButton.h"

#import "UHColor.h"



@interface ColorViewController : UIViewController

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *randomizeButtonHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *randomizeButtonWidthConstraint;







//GESTURES
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@property (nonatomic, strong) UITapGestureRecognizer *doubleTapGesture;
@property (strong, nonatomic) UIPinchGestureRecognizer *pinchGesture;
@property (strong, nonatomic) UILongPressGestureRecognizer *longGesture;
@property (strong, nonatomic) UISwipeGestureRecognizer *swipeLeftGesture;
@property (strong, nonatomic) UISwipeGestureRecognizer *swipeUpGesture;
@property (strong, nonatomic) UIPanGestureRecognizer *swipeRightGesture;
@property (strong, nonatomic) UISwipeGestureRecognizer *swipeDownGesture;



@property (strong, nonatomic) UHColor* viewToTap;
@property (strong,nonatomic) UHColor* editableSliderViewColor;

@property (weak, nonatomic) IBOutlet UIButton *showTutorialButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *randomizeAllButton;

@property (strong, nonatomic) UISlider *redSlider;
@property (strong, nonatomic) UILabel *redSliderLabel;


@property (strong, nonatomic) UISlider *greenSlider;
@property (strong, nonatomic) UILabel *greenSliderLabel;


@property (strong, nonatomic) UISlider *blueSlider;
@property (strong, nonatomic) UILabel *blueSliderLabel;


@property (nonatomic) float redSliderValue;
@property (nonatomic) float greenSliderValue;
@property (nonatomic) float blueSliderValue;

@property (nonatomic)int blurNumber;



@property NSMutableArray *colorViews;

@property UILabel *hexLabel;
@property UILabel *stringCopiedLabel;


@property UIVisualEffectView *vibrancyEffectView;
@property UIVisualEffectView *blurView;

@property UIVisualEffectView *detailVibrancyView;
@property UIVisualEffectView *detailBlurView;

@property UIVisualEffectView *sliderView;


@property UIView *detailView;
@property UIDynamicAnimator *dynamicAnimator;
@property UICollisionBehavior *collisionBehavior;
@property UIGravityBehavior *gravityBehavior;
@property UIDynamicItemBehavior *dynamicItemBehavior;
@property UIPushBehavior *pushBehavior;

@property NSMutableArray *items;

- (IBAction)saveButtonPressed:(UIButton *)sender;
- (IBAction)settingsButtonPressed:(UIButton *)sender;
- (IBAction)randomizeAllButtonPressed:(UIButton *)sender;
- (IBAction)randomizeAllButtonHasTap:(UIButton *)sender;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
- (IBAction)randomizeRepeat:(UIButton *)sender;


@property UIVisualEffectView *tutorialVibrancyView;
@property UIVisualEffectView *tutorialBlurView;
@property UILabel *firstLineLabel;

@property (strong, nonatomic)NSUserDefaults *defaults;
@property (nonatomic)BOOL tutorial;



@end

