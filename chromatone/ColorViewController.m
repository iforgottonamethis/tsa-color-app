#import "ColorViewController.h"
#import "UHColor.h"
#import <QuartzCore/QuartzCore.h>

#import "Colours.h"
//#import "UIView+FLKAutoLayout.h"




@interface ColorViewController () <UICollisionBehaviorDelegate>
{
    UHColor *colorClass;
}
@property int totalColorsAdded;
@property CGFloat randomButtonHeightConstantSize;
@property CGFloat randomButtonWidthConstantSize;

@property (strong,nonatomic) UIView *colorViewBackground;


@end

@implementation ColorViewController
@synthesize
colorViewBackground,
totalColorsAdded,
defaults,
tutorial,
tutorialBlurView,
firstLineLabel,
tapGesture,
doubleTapGesture,
pinchGesture,
longGesture,
swipeLeftGesture,
swipeRightGesture,
swipeUpGesture,
sliderView,
blurNumber,
colorViews,
detailView,
dynamicAnimator,
gravityBehavior,
collisionBehavior,
dynamicItemBehavior,
pushBehavior,
items,
blurView,
vibrancyEffectView,
redSlider,
blueSlider,
greenSlider,
redSliderValue,
greenSliderValue,
blueSliderValue,
hexLabel,
randomButtonHeightConstantSize,
randomButtonWidthConstantSize,
redSliderLabel,
greenSliderLabel,
blueSliderLabel,
detailBlurView,
detailVibrancyView,
swipeDownGesture;

-(void)viewDidAppear:(BOOL)animated
{
    [self prefersStatusBarHidden];
    [super viewDidAppear:YES];

    
    blurView.alpha = 0;
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    
    tutorialBlurView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    tutorialBlurView.frame = self.view.bounds;
    
    detailBlurView = [[UIVisualEffectView alloc]initWithEffect:blurEffect];
    
    
    
    
    
    
    UITapGestureRecognizer *dismissalTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped:)];
    dismissalTap.numberOfTapsRequired = 1;



    
    
    self.view.backgroundColor = [UIColor clearColor];
    
    UIBlurEffect *tutorialBlurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVibrancyEffect *vibrantBlur = [UIVibrancyEffect effectForBlurEffect:tutorialBlurEffect];
    
    self.editableSliderViewColor = [[UHColor alloc]initWithFrame:CGRectMake(0,0,self.view.frame.size.width,150)];
    self.editableSliderViewColor.alpha = 0;
    
    
    vibrancyEffectView = [[UIVisualEffectView alloc] initWithEffect:vibrantBlur];
    vibrancyEffectView.frame = self.view.bounds;
    vibrancyEffectView.alpha = 0;
    
    
    
    blurView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    blurView.frame = self.view.bounds;
    //    blurView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    blurView.alpha = 0;
    
    [[blurView contentView] addSubview:vibrancyEffectView];
    
    
    sliderView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
    sliderView.frame = CGRectMake(0, self.view.frame.size.height / 2, self.view.frame.size.width, 150);
    //self.sliderView.backgroundColor = [UIColor whiteColor];
    sliderView.alpha = 0;
    
    
    
    //    UHTutorial *tutorialStuff = [[UHTutorial alloc]init];
    //    [tutorialStuff firstTimeTutorial:self forView:self.view];
    
    
    redSlider = [[UISlider alloc]init];
    [redSlider addTarget:self action:@selector(redSliderValueChanging:) forControlEvents:UIControlEventValueChanged];
    redSlider.frame = CGRectMake(10, 10, 225, 20);
    redSlider.alpha = 0;
    redSlider.minimumValue = 1;
    redSlider.maximumValue = 255;
    redSlider.tintColor = [UIColor redColor];
    //    redSlider.thumbTintColor = [UIColor pinkColor];
    
    redSliderLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width - 45, redSlider.frame.origin.y, 35, redSlider.frame.size.height)];

    redSliderLabel.textAlignment = NSTextAlignmentCenter;
    redSliderLabel.userInteractionEnabled = YES;
    redSliderLabel.textColor = [UIColor whiteColor];
    
    UITapGestureRecognizer *redLabelTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(redSliderLabelTapped:)];
    redLabelTapGesture.numberOfTapsRequired = 1;
    [redSliderLabel addGestureRecognizer:redLabelTapGesture];
    
    
    
    
    
    
    greenSlider = [[UISlider alloc]init];
    [greenSlider addTarget:self action:@selector(greenSliderValueChanging:) forControlEvents:UIControlEventValueChanged];
    greenSlider.frame = CGRectMake(10,65, 225, 20);
    greenSlider.alpha = 0;
    greenSlider.minimumValue = 1;
    greenSlider.maximumValue = 255;
    greenSlider.tintColor = [UIColor greenColor];
    
    
    greenSliderLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width - 45, greenSlider.frame.origin.y, 35, redSlider.frame.size.height)];
    greenSliderLabel.textAlignment = NSTextAlignmentCenter;
    greenSliderLabel.userInteractionEnabled = YES;
    greenSliderLabel.textColor = [UIColor whiteColor];
    
    UITapGestureRecognizer *greenLabelTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(greenSliderLabelTapped:)];
    greenLabelTapGesture.numberOfTapsRequired = 1;
    [greenSliderLabel addGestureRecognizer:greenLabelTapGesture];
    
    
    
    blueSlider = [[UISlider alloc]init];
    [blueSlider addTarget:self action:@selector(blueSliderValueChanging:) forControlEvents:UIControlEventValueChanged];
    blueSlider.frame = CGRectMake(10,120, 225, 20);
    blueSlider.alpha = 0;
    blueSlider.minimumValue = 1;
    blueSlider.maximumValue = 255;
    blueSlider.tintColor = [UIColor skyBlueColor];
//    [blueSlider setTintAdjustmentMode:UIViewTintAdjustmentModeAutomatic];
    
    
    blueSliderLabel = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width - 45, blueSlider.frame.origin.y, 35, blueSlider.frame.size.height)];
    blueSliderLabel.textAlignment = NSTextAlignmentCenter;
    blueSliderLabel.userInteractionEnabled = YES;
    blueSliderLabel.textColor = [UIColor whiteColor];
    
    UITapGestureRecognizer *blueLabelTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(blueSliderLabelTapped:)];
    greenLabelTapGesture.numberOfTapsRequired = 1;
    [blueSliderLabel addGestureRecognizer:blueLabelTapGesture];
    
    
    
    UHButton *buttonStuff = [[UHButton alloc]init];
    [buttonStuff setUpButton:self.randomizeAllButton];
    [buttonStuff setUpButton:self.saveButton];
    [self.randomizeAllButton setImage:[UIImage imageNamed:@"random"] forState:UIControlStateNormal];
    //[self.randomizeAllButton setImage:[UIImage imageNamed:@"random_interaction"] forState:UIControlStateHighlighted];

    [self.saveButton setImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
    
        [self.saveButton setImage:[UIImage imageNamed:@"share_interaction"] forState:UIControlStateHighlighted];
    tapGesture.enabled = YES;
    pinchGesture.enabled = YES;
    longGesture.enabled = YES;
    swipeLeftGesture.enabled = YES;
    doubleTapGesture.enabled = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    blurView.alpha = 0;
    colorClass = [UHColor new];
 


    colorViewBackground = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:colorViewBackground];
    
    
    
    // basically make a uiview the size of the frame
//    UHColor *firstView = [[UHColor alloc] initWithFrame:self.view.frame];
//    UHColor *firstView = [[UHColor alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)]; //AD FRAME

    UHColor *firstView = [[UHColor alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, colorViewBackground.frame.size.height)];//    [self getRandomColorAndHexForColorView:firstView];
    [colorClass setRandomColorForView:firstView];
//    firstView.frame = self.view.frame;
//    [self.view addSubview:firstView];
    [colorViewBackground addSubview:firstView];
    
    //init array with the above view inside
    colorViews = [NSMutableArray arrayWithObject:firstView];
    
    //for the details such as hex
    self.detailView = [[UIView alloc]init];//initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    detailView.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, colorViewBackground.frame.size.height);
    //    self.detailView.backgroundColor = [UIColor whiteColor];
    self.detailView.alpha = 0.0;
    [colorViewBackground addSubview:self.detailView];
    
//    [detailView.layer setShadowColor:[UIColor darkGrayColor].CGColor];
//    [detailView.layer setShadowOffset:CGSizeMake(4, 0)];
//    [detailView.layer setShadowOpacity:.5];
    
    
    
    
    
    //GESTURES
#pragma mark - Gesture Creation
    
    
    tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedView:)];
    tapGesture.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tapGesture];
    
    
    
    
    doubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTap:)];
    doubleTapGesture.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:doubleTapGesture];
    
    [tapGesture requireGestureRecognizerToFail:doubleTapGesture];
    
    
    
    pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchedView:)];
    [self.view addGestureRecognizer:pinchGesture];
    
    
    
    longGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressed:)];
    [self.view addGestureRecognizer:longGesture];
    
    
    
    swipeLeftGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeftToDelete:)];
    swipeLeftGesture.direction = UISwipeGestureRecognizerDirectionLeft | UISwipeGestureRecognizerDirectionRight;

    [self.view addGestureRecognizer:swipeLeftGesture];
    


    
    
    
    
    swipeUpGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeUpView:)];
    swipeUpGesture.direction = UISwipeGestureRecognizerDirectionUp;
    [swipeUpGesture setDirection:UISwipeGestureRecognizerDirectionUp];
    [self.view addGestureRecognizer:swipeUpGesture];
    
    swipeDownGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeDownToDismissGesture:)];
    swipeDownGesture.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeDownGesture];

    
    
    
}


#pragma mark - <MPAdViewDelegate>
- (UIViewController *)viewControllerForPresentingModalView {
    return self;
}



#pragma mark - IBActions


- (IBAction)randomizeAllButtonPressed:(UIButton *)sender {
    UHButton *hideButton = [[UHButton alloc]init];
    [self.randomizeAllButton setImage:[UIImage imageNamed:@"random_interaction"] forState:UIControlStateHighlighted];


        if (randomButtonWidthConstantSize == self.randomizeButtonWidthConstraint.constant && randomButtonHeightConstantSize == self.randomizeButtonHeightConstraint.constant) {
            self.randomizeButtonHeightConstraint.constant = self.randomizeButtonHeightConstraint.constant * .25;
            self.randomizeButtonWidthConstraint.constant = self.randomizeButtonWidthConstraint.constant * .25;
        }else{
            self.randomizeButtonHeightConstraint.constant = self.randomizeButtonHeightConstraint.constant / .25;
            self.randomizeButtonWidthConstraint.constant = self.randomizeButtonWidthConstraint.constant / .25;
        }


        blurView.alpha = 0.0f;
        double delayInSeconds = 1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            // code to be executed on the main queue after delay
            tapGesture.enabled = YES;
            pinchGesture.enabled = YES;
            longGesture.enabled = YES;
            swipeLeftGesture.enabled = YES;
            doubleTapGesture.enabled = YES;
            
        });
        
        [hideButton hideButton:self.saveButton];
        [hideButton hideButton:self.randomizeAllButton];
        
        
        
        for (int i = 0; i < [colorViews count]; i++) {
            UHColor *view = self.colorViews[i];
//            [self getRandomColorAndHexForColorView:view];
            
            [colorClass setRandomColorForView:view];
        }
    
    
}

- (IBAction)randomizeAllButtonHasTap:(UIButton *)sender {
            [self.randomizeAllButton setImage:[UIImage imageNamed:@"random_interaction"] forState:UIControlStateHighlighted];
                         if (randomButtonWidthConstantSize == self.randomizeButtonWidthConstraint.constant && randomButtonHeightConstantSize == self.randomizeButtonHeightConstraint.constant) {
                             self.randomizeButtonHeightConstraint.constant = self.randomizeButtonHeightConstraint.constant * .25;
                             self.randomizeButtonWidthConstraint.constant = self.randomizeButtonWidthConstraint.constant * .25;
                         }else{
                             self.randomizeButtonHeightConstraint.constant = self.randomizeButtonHeightConstraint.constant / .25;
                             self.randomizeButtonWidthConstraint.constant = self.randomizeButtonWidthConstraint.constant / .25;
                         }
}

- (IBAction)randomizeAllButtonCanceled:(UIButton *)sender {
    if (!(randomButtonWidthConstantSize == self.randomizeButtonWidthConstraint.constant && randomButtonHeightConstantSize == self.randomizeButtonHeightConstraint.constant)) {
        self.randomizeButtonHeightConstraint.constant = self.randomizeButtonHeightConstraint.constant / .25;
        self.randomizeButtonWidthConstraint.constant = self.randomizeButtonWidthConstraint.constant / .25;
    }

}



- (IBAction)randomizeRepeat:(UIButton *)sender {
            [self.randomizeAllButton setImage:[UIImage imageNamed:@"random_interaction"] forState:UIControlStateHighlighted];

                         if (randomButtonWidthConstantSize == self.randomizeButtonWidthConstraint.constant && randomButtonHeightConstantSize == self.randomizeButtonHeightConstraint.constant) {
                             self.randomizeButtonHeightConstraint.constant = self.randomizeButtonHeightConstraint.constant * .25;
                             self.randomizeButtonWidthConstraint.constant = self.randomizeButtonWidthConstraint.constant * .25;
                         }else{
                             self.randomizeButtonHeightConstraint.constant = self.randomizeButtonHeightConstraint.constant / .25;
                             self.randomizeButtonWidthConstraint.constant = self.randomizeButtonWidthConstraint.constant / .25;
                         }
}

-(IBAction)saveButtonPressed:(UIButton *)sender
{

    if (detailView.alpha == 0) {
        for (UHColor * colorView  in self.colorViews) {
            hexLabel = [[UILabel alloc] initWithFrame:CGRectMake(colorView.frame.origin.x, colorView.frame.origin.y , colorView.frame.size.width, colorView.frame.size.height)];
            hexLabel.text = [NSString stringWithFormat:@"red: %@ green: %@ blue: %@ \n hex: %@", colorView.redColor, colorView.greenColor, colorView.blueColor, colorView.hexString];
            hexLabel.textAlignment = NSTextAlignmentCenter;
            hexLabel.font = [UIFont fontWithName:@"Roboto" size:32.0];
            
            hexLabel.numberOfLines = 0;
            hexLabel.alpha = 1;
            
            int bgDelta = (([colorView.redColor floatValue] * 0.299) + ([colorView.greenColor floatValue] * 0.587) + ([colorView.blueColor floatValue] * 0.114));
            
            
            int threshold = 105;
            
            
            UIColor *textColor = (255 - bgDelta < threshold) ? [UIColor blackColor] : [UIColor whiteColor];
            hexLabel.textColor = textColor;
            
            [self.detailView addSubview:hexLabel];
        }
        [UIView animateWithDuration:.5 animations:^{
            blurView.alpha = 0.0;
            
        }];
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            UHButton *hideButton = [[UHButton alloc]init];
            [hideButton hideButton:self.saveButton];

            [hideButton hideButton:self.randomizeAllButton];
            blurView.alpha = 0.0;
            self.detailView.alpha = 0.5;
            
            [self.detailView.subviews setValue:@1.0 forKey:@"alpha"];
        } completion:nil];
        
          UIView *wholeScreen = colorViewBackground;
         
         // define the size and grab a UIImage from it
         UIGraphicsBeginImageContextWithOptions(wholeScreen.bounds.size, wholeScreen.opaque, 0.0);
         [wholeScreen.layer renderInContext:UIGraphicsGetCurrentContext()];
         UIImage *screengrab = UIGraphicsGetImageFromCurrentImageContext();
         UIGraphicsEndImageContext();
//         UIImageWriteToSavedPhotosAlbum(screengrab, nil, nil, nil);
        
        NSString *message = [NSString stringWithFormat:@"Check out this awesome color palette from the app Shades!"];
        NSArray *activityVCItems = @[message,screengrab];
        UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:activityVCItems applicationActivities:nil];
                                                [self presentViewController:activityVC animated:YES completion:nil];


        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.detailView.alpha = 0.0;
            
            [self.detailView.subviews setValue:@0 forKey:@"alpha"];
        } completion:^(BOOL finished) {
            [self.detailView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
            
        }];
        

        
        
        
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Image Saved!" preferredStyle:UIAlertControllerStyleAlert];
//        double delayInSeconds = 1.0;
//        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//            [self presentViewController:alert animated:YES completion:nil];
//            
//        });
//        
//        UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
//            [alert dismissViewControllerAnimated:YES completion:nil];
//        }];
//        [alert addAction:dismiss];
//        
    }else{
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.detailView.alpha = 0.0;
            [self.detailView.subviews setValue:@0 forKey:@"alpha"];
        } completion:^(BOOL finished) {
            [self.detailView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        }];
        
    }
    
    
    
}
-(IBAction)settingsButtonPressed:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"segue" sender:self];

    
    
    
    
}

    





-(void)redSliderValueChanging:(UISlider *)slider{
    UHColor  *color = self.editableSliderViewColor;
    
    color.redColor = [NSString stringWithFormat:@"%.f",redSliderValue];
    
    
    color.hexString = [NSString stringWithFormat:@"%X%X%X",[[NSNumber numberWithFloat: redSliderValue] intValue],[color.greenColor intValue],[color.blueColor intValue]];
    
    
    
    redSliderValue = lroundf(slider.value);
    [colorClass displayColorView:color withColor:[UIColor colorWithRed:redSliderValue / 255 green:greenSlider.value / 255 blue:blueSlider.value / 255 alpha:1]];
    redSliderLabel.text = [NSString stringWithFormat:@"%.f",redSlider.value];
    
}



-(void)greenSliderValueChanging:(UISlider *)slider{
    UHColor  *color = self.editableSliderViewColor;
    
    color.greenColor = [NSString stringWithFormat:@"%.f",greenSliderValue];
    
    color.hexString = [NSString stringWithFormat:@"%X%X%X",[color.redColor intValue],[[NSNumber numberWithFloat:greenSliderValue]intValue],[color.blueColor intValue]];
    
    greenSliderValue = lroundf(slider.value);
    
    
    
    [colorClass displayColorView:color withColor:[UIColor colorWithRed:redSlider.value / 255 green:greenSliderValue / 255 blue:blueSlider.value / 255 alpha:1]];
    greenSliderLabel.text = [NSString stringWithFormat:@"%.f",greenSlider.value];

    
}

-(void)blueSliderValueChanging:(UISlider *)slider{
    UHColor  *color = self.editableSliderViewColor;
    
    color.blueColor = [NSString stringWithFormat:@"%.f",blueSliderValue];
    
    color.hexString = [NSString stringWithFormat:@"%X%X%X",[color.redColor intValue],[color.greenColor intValue],[[NSNumber numberWithFloat:blueSliderValue]intValue]];
    
    
    blueSliderValue = lroundf(slider.value);
    [colorClass displayColorView:color withColor:[UIColor colorWithRed:redSlider.value / 255 green:greenSlider.value  / 255 blue:blueSliderValue / 255 alpha:1]];
    blueSliderLabel.text = [NSString stringWithFormat:@"%.f",blueSlider.value];

}



#pragma mark - GESTURES

-(void)labelTapped:(UITapGestureRecognizer *)tap
{
    
    [tutorialBlurView setAlpha:0];
    [firstLineLabel setAlpha:0];
}


-(void)swipeUpView:(UIGestureRecognizer *)tap
{
    UHButton *buttonStuff = [[UHButton alloc]init];
    
    if (self.randomizeAllButton.alpha == 0 && !UIAccessibilityIsReduceTransparencyEnabled()) {
        [colorViewBackground addSubview:blurView];
        [UIView animateWithDuration:.25 animations:^{
            [buttonStuff showHideButtons:self.randomizeAllButton andAddToSubview:self.view];
            [buttonStuff showHideButtons:self.saveButton andAddToSubview:self.view];
            self.randomizeAllButton.alpha = 1;
            blurView.alpha = 1;
            vibrancyEffectView.alpha = 1;
            tapGesture.enabled = NO;
            pinchGesture.enabled = NO;
            longGesture.enabled = NO;
            swipeLeftGesture.enabled = NO;
            doubleTapGesture.enabled = NO;
        }];
    }else if (self.randomizeAllButton.alpha == 0 && UIAccessibilityIsReduceTransparencyEnabled()){
        [UIView animateWithDuration:.25 animations:^{
            [buttonStuff showHideButtons:self.randomizeAllButton andAddToSubview:self.view];
            [buttonStuff showHideButtons:self.saveButton andAddToSubview:self.view];
            self.view.backgroundColor = [UIColor blackColor];
            tapGesture.enabled = NO;
            pinchGesture.enabled = NO;
            longGesture.enabled = NO;
            swipeLeftGesture.enabled = NO;
            doubleTapGesture.enabled = NO;
        }];
    }else if (self.randomizeAllButton.alpha == 1 && !UIAccessibilityIsReduceTransparencyEnabled() && self.blurView.alpha == 1){
        [UIView animateWithDuration:.25 animations:^{
            [buttonStuff showHideButtons:self.randomizeAllButton andAddToSubview:self.view];
            [buttonStuff showHideButtons:self.saveButton andAddToSubview:self.view];
            blurView.alpha = 0;
            vibrancyEffectView.alpha = 0;
            tapGesture.enabled = YES;
            pinchGesture.enabled = YES;
            longGesture.enabled = YES;
            swipeLeftGesture.enabled = YES;
            doubleTapGesture.enabled = YES;
        }];
    }else if (self.randomizeAllButton.alpha == 1 && blurView.alpha == 0){
        [UIView animateWithDuration:.25 animations:^{
            [self.view addSubview:blurView];
            blurView.alpha = 1;
            tapGesture.enabled = NO;
            pinchGesture.enabled = NO;
            longGesture.enabled = NO;
            swipeLeftGesture.enabled = NO;
            doubleTapGesture.enabled = NO;
        }];
    }else if (!(self.randomizeAllButton.alpha == 1 && blurView.alpha == 0)){
        [UIView animateWithDuration:.25 animations:^{
            self.blurView.alpha = 0;
            tapGesture.enabled = YES;
            pinchGesture.enabled = YES;
            longGesture.enabled = YES;
            swipeLeftGesture.enabled = YES;
            doubleTapGesture.enabled = YES;
        }];
    }else if ((self.randomizeAllButton.alpha==1) && (self.blurView.alpha == 1)){
        UHButton *buttonStuff = [[UHButton alloc]init];
        
        [UIView animateWithDuration:.25 animations:^{
            [buttonStuff hideButton:self.randomizeAllButton];
            [buttonStuff hideButton:self.saveButton];
            blurView.alpha = 0;
            vibrancyEffectView.alpha = 0;
            tapGesture.enabled = YES;
            pinchGesture.enabled = YES;
            longGesture.enabled = YES;
            swipeLeftGesture.enabled = YES;
            doubleTapGesture.enabled = YES;
            NSLog(@"%f",blurView.alpha);
        }];
    }else{
        
        [UIView animateWithDuration:.25 animations:^{

            blurView.alpha = 0;
            vibrancyEffectView.alpha = 0;
            tapGesture.enabled = YES;
            pinchGesture.enabled = YES;
            longGesture.enabled = YES;
            swipeLeftGesture.enabled = YES;
            doubleTapGesture.enabled = YES;
            NSLog(@"%f",blurView.alpha);
        }];
    }
    
    
}





-(void)doubleTap:(UIGestureRecognizer*)tap
{
    
    CGPoint tapPoint = [tap locationInView:self.view];
    UHColor  *tappedView = [self findViewUsingPoint:tapPoint];
    
    
    
    if (redSlider.alpha == 0 && tappedView && (blurView.alpha == 0)) {
        [UIView animateWithDuration:.35 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.swipeDownGesture.enabled = NO;
            
            UHButton *buttonStuff = [[UHButton alloc]init];
//            [buttonStuff showHideButtons:self.randomizeAllButton andAddToSubview:self.view];
//            [buttonStuff showHideButtons:self.saveButton andAddToSubview:self.view];
            [buttonStuff hideButton:self.randomizeAllButton];
            [buttonStuff hideButton:self.saveButton];

            
            self.viewToTap = tappedView;
            tapGesture.enabled = NO;
            pinchGesture.enabled = NO;
            longGesture.enabled = NO;
            swipeLeftGesture.enabled = NO;
            swipeUpGesture.enabled = NO;
            
            
            [blurView addSubview:self.editableSliderViewColor];
            self.editableSliderViewColor.alpha = 1;
            [colorClass displayColorView:self.editableSliderViewColor withColor:[UIColor colorWithRed:[tappedView.redColor floatValue]/255.0f green:[tappedView.greenColor floatValue]/255.0f blue:[tappedView.blueColor floatValue]/255.0f alpha:1.0f]];
            [blurView bringSubviewToFront:self.editableSliderViewColor];
            
            
            [colorViewBackground addSubview:blurView];
            blurView.alpha = 1;
            vibrancyEffectView.alpha = 1;
            [colorViewBackground addSubview:blurView];
            
            [blurView addSubview:sliderView];
            sliderView.alpha = .75;
            
            [blurView addSubview:sliderView];
            [blurView bringSubviewToFront:sliderView];
            
            
            
            [sliderView addSubview:redSlider];
            
            [sliderView addSubview:redSliderLabel];
            redSliderLabel.alpha = 1;
            
            
            redSlider.alpha = 1;
            redSlider.value = [tappedView.redColor floatValue];
            redSliderLabel.text = [NSString stringWithFormat:@"%.f",redSlider.value];
            
            
            [sliderView addSubview:greenSliderLabel];
            greenSliderLabel.alpha = 1;
            
            
            
            [sliderView addSubview:greenSlider];
            greenSlider.alpha = 1;
            greenSlider.value = [tappedView.greenColor floatValue];
            greenSliderLabel.text = [NSString stringWithFormat:@"%.f",greenSlider.value];
            
            
            [sliderView addSubview:blueSliderLabel];
            blueSliderLabel.alpha = 1;
            
            [sliderView addSubview:blueSlider];
            blueSlider.alpha = 1;
            blueSlider.value = [tappedView.blueColor floatValue];
            blueSliderLabel.text = [NSString stringWithFormat:@"%.f",blueSlider.value];
        }
        
        
        completion:nil];
        


    }else{
        
        [UIView animateWithDuration:.35 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.swipeDownGesture.enabled = YES;
            
            self.viewToTap.redColor = [NSString stringWithFormat:@"%.f",self.redSlider.value];//self.editableSliderViewColor.redColor;
            self.viewToTap.greenColor =  [NSString stringWithFormat:@"%.f",self.greenSlider.value]; //self.editableSliderViewColor.greenColor;
            self.viewToTap.blueColor =  [NSString stringWithFormat:@"%.f",self.blueSlider.value];//self.editableSliderViewColor.blueColor;
            self.viewToTap.hexString = [NSString stringWithFormat:@"%X%X%X",[self.viewToTap.redColor intValue],[self.viewToTap.greenColor intValue],[self.viewToTap.blueColor intValue]];
            
            
            [colorClass displayColorView:self.viewToTap withColor:[UIColor colorWithRed:redSlider.value/255.0f green:greenSlider.value/255.0f blue:blueSlider.value / 255.0f alpha:1.0f]];
            
            
            
            self.editableSliderViewColor.alpha = 0;
            
            blurView.alpha = 0;
            UHButton *buttonStuff = [[UHButton alloc]init];
            //        [buttonStuff showHideButtons:self.randomizeAllButton andAddToSubview:self.view];
            //        [buttonStuff showHideButtons:self.saveButton andAddToSubview:self.view];
            [buttonStuff hideButton:self.randomizeAllButton];
            [buttonStuff hideButton:self.saveButton];
            
            sliderView.alpha = 0;
            
            [redSlider removeFromSuperview];
            redSlider.alpha = 0;
            
            [greenSlider removeFromSuperview];
            greenSlider.alpha = 0;
            
            [blueSlider removeFromSuperview];
            blueSlider.alpha = 0;
            
            
            
            tapGesture.enabled = YES;
            pinchGesture.enabled = YES;
            longGesture.enabled = YES;
            swipeLeftGesture.enabled = YES;
            swipeUpGesture.enabled = YES;
        }completion:nil];

        
    }
    
    
    
    
    
    
    //    [self displayColorView:tappedView withColor:[UIColor blackColor]];
    
    
    
    
    
    
    
    
    
    
    
    
    
    //    if (tappedView && detailView.alpha == 0) {
    //        for (UHColor  *UHColor  in self.colorViews) {
    //            UILabel *hexLabel = [[UILabel alloc] initWithFrame:CGRectMake(colorView.frame.origin.x, colorView.frame.origin.y, colorView.frame.size.width / 2, colorView.frame.size.height)];
    //            hexLabel.text = [NSString stringWithFormat:@"red: %@ green: %@ blue: %@ \n hue: %@ saturation: %@ value: %@ \n hex: %@", colorView.redColor, colorView.greenColor, colorView.blueColor, colorView.hueColor, colorView.saturationColor, colorView.brightnessColor, colorView.hexString];
    //            hexLabel.textAlignment = NSTextAlignmentCenter;
    //            hexLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11.0];
    //            hexLabel.textColor = [UIColor blackColor];
    //            hexLabel.numberOfLines = 0;
    //            hexLabel.alpha = 1;
    //            [self.detailView addSubview:hexLabel];
    //
    //
    //
    //        }
    //        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
    //            self.detailView.alpha = 0.5;
    //            [self.detailView.subviews setValue:@1.0 forKey:@"alpha"];
    //        } completion:nil];
    //    }else{
    //
    //            [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
    //                self.detailView.alpha = 0.0;
    //                [self.detailView.subviews setValue:@1.0 forKey:@"alpha"];
    //            } completion:^(BOOL finished) {
    //                [self.detailView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    //            }];
    //        }
    //
    //
    
    
}



- (void)tappedView:(UITapGestureRecognizer *)tap
{
    CGPoint tapPoint = [tap locationInView:self.view];
    UHColor  *tappedView = [self findViewUsingPoint:tapPoint];
    //    UHTutorial *tutorialStuff = [UHTutorial new];
    //    if (tutorialStuff.firstLineLabel.alpha == 1) {
    //        [tutorialStuff.firstLineLabel removeFromSuperview];
    //        [tutorialStuff.firstLineLabel setAlpha:0];
    //
    //    }
    if (tappedView) {
        tapGesture.enabled = YES;
        pinchGesture.enabled = YES;
        longGesture.enabled = YES;
        swipeLeftGesture.enabled = YES;
        swipeUpGesture.enabled = YES;
        doubleTapGesture.enabled = YES;
        
        //        [self getRandomColorAndHexForColorView:tappedView];

        UHButton *buttonStuff = [[UHButton alloc]init];
        [UIView animateWithDuration:.5 animations:^{
            self.detailView.alpha = 0.0;
            self.blurView.alpha = 0.0;
            [self.detailView.subviews setValue:@0.0 forKey:@"alpha"];
            
            
            
            [buttonStuff hideButton:self.saveButton];
            [buttonStuff hideButton:self.randomizeAllButton];
            blurView.alpha = 0;
            
            redSlider.alpha = 0;
            greenSlider.alpha = 0;
            blueSlider.alpha = 0;
            
            
            
            [blurView removeFromSuperview];
            [greenSlider removeFromSuperview];
            [redSlider removeFromSuperview];
            [blueSlider removeFromSuperview];
            
            
//            [self getRandomColorAndHexForColorView:tappedView];
            }];
        [colorClass setRandomColorForView:tappedView];

        
    }
}

- (void)pinchedView:(UIPinchGestureRecognizer *)pinch
{
    if (pinch.state == UIGestureRecognizerStateBegan) {
        
        
        if ([colorViews count] <= 7 && blurView.alpha == 0) {
            
            
            
            UHColor  *newView = [[UHColor  alloc] initWithFrame:CGRectMake(0, self.colorViews.count  * colorViewBackground.frame.size.height / (self.colorViews.count +1), self.view.frame.size.width, (colorViewBackground.frame.size.height) / (self.colorViews.count))];
            
            [colorViewBackground addSubview:newView];
            newView.frame = CGRectMake(0, self.colorViews.count  * colorViewBackground.frame.size.height / (self.colorViews.count +1), self.view.frame.size.width, colorViewBackground.frame.size.height / (self.colorViews.count));
            [colorClass setRandomColorForView:newView];


            
            [UIView animateWithDuration:0.15 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                self.detailView.alpha = 0.0;
                [self.detailView.subviews setValue:@1.0 forKey:@"alpha"];
            } completion:nil];
            
            for (int i = 0; i < [colorViews count]; i++) {
                UHColor  *view = self.colorViews[i];
                view.frame = CGRectMake(0, i * colorViewBackground.frame.size.height / (self.colorViews.count + 1), self.view.frame.size.width, (colorViewBackground.frame.size.height) / (self.colorViews.count + 1));
            }
            
            [self.colorViews addObject:newView];
            for (UHColor *colorView in colorViews){
                if (colorView.redColor == newView.redColor && colorView.greenColor == newView.greenColor &&colorView.blueColor == newView.blueColor) {
					[colorClass setRandomColorForView:newView];
                }
            }

            [colorViewBackground bringSubviewToFront:self.detailView];
            
            totalColorsAdded++;
        }
        
    }
}

- (void)longPressed:(UILongPressGestureRecognizer *)press
{
    if (press.state == UIGestureRecognizerStateBegan) {
        for (UHColor * colorView  in self.colorViews) {
            if (colorViews.count < 3 && totalColorsAdded <= 2) {
                hexLabel = [[UILabel alloc] initWithFrame:CGRectMake(colorView.frame.origin.x, colorView.frame.origin.y - (colorView.frame.origin.y / 2) , colorView.frame.size.width, colorView.frame.size.height)];
                
            }else{
                hexLabel = [[UILabel alloc] initWithFrame:CGRectMake(colorView.frame.origin.x, colorView.frame.origin.y , colorView.frame.size.width, colorView.frame.size.height)];
                
            }
            hexLabel.text = [NSString stringWithFormat:@"red: %@ green: %@ blue: %@ \n hex: %@", colorView.redColor, colorView.greenColor, colorView.blueColor, colorView.hexString];
            hexLabel.textAlignment = NSTextAlignmentCenter;
            hexLabel.font = [UIFont fontWithName:@"Roboto" size:32.0];
            
            hexLabel.numberOfLines = 0;
            hexLabel.alpha = 1;
            
            int bgDelta = (([colorView.redColor floatValue] * 0.299) + ([colorView.greenColor floatValue] * 0.587) + ([colorView.blueColor floatValue] * 0.114));
            
            
            int threshold = 105;
            
            
            UIColor *textColor = (255 - bgDelta < threshold) ? [UIColor blackColor] : [UIColor whiteColor];
            hexLabel.textColor = textColor;
            
            [self.detailView addSubview:hexLabel];
        }
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.detailView.alpha = 0.5;
            [self.detailView.subviews setValue:@1.0 forKey:@"alpha"];
        } completion:nil];
    } else if (press.state == UIGestureRecognizerStateEnded) {
        [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            self.detailView.alpha = 0.0;
            [self.detailView.subviews setValue:@0 forKey:@"alpha"];
        } completion:^(BOOL finished) {
            [self.detailView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        }];
    }
    
}

-(void)swipeLeftToDelete:(UISwipeGestureRecognizer *)swipe
{
    CGPoint swipePoint = [swipe locationInView:self.view];
    UHColor  *tappedView = [self findViewUsingPoint:swipePoint];
    
    if (tappedView) {
        
        [UIView animateWithDuration:.50 delay:0 options:UIViewAnimationOptionTransitionCurlUp animations:^{
            
            if (colorViews.count < 2) {
                UHColor  *firstView = [[UHColor  alloc] initWithFrame:colorViewBackground.frame];
//                [self getRandomColorAndHexForColorView:firstView];
                [colorClass setRandomColorForView:firstView];
                [colorViewBackground addSubview:firstView];
                totalColorsAdded = 0;
                
                colorViews = [NSMutableArray arrayWithObject:firstView];
                
                UIView *random = [[UIView alloc] initWithFrame:self.view.frame];
                random.backgroundColor = [UIColor blackColor];
                random.alpha = 0.0;
                [self.view addSubview:random];
                [self.view bringSubviewToFront:detailView];
                
                
            }else{
                
                [tappedView removeFromSuperview];
                [colorViews removeObject:tappedView];
                
                for (int i = 0; i < [colorViews count]; i++) {
                    UHColor  *view = self.colorViews[i];
                    view.frame = CGRectMake(0, i * colorViewBackground.frame.size.height / (self.colorViews.count), self.view.frame.size.width, colorViewBackground.frame.size.height / (self.colorViews.count));
                }
            }
        }
                         completion:nil];
    }
    
    
    
}

-(void)swipeDownToDismissGesture:(UISwipeGestureRecognizer *)swipe{
    UHButton *buttonStuff = [[UHButton alloc]init];

    [UIView animateWithDuration:.25 animations:^{
        [buttonStuff hideButton:self.randomizeAllButton];
        [buttonStuff hideButton:self.saveButton];
        blurView.alpha = 0;
        vibrancyEffectView.alpha = 0;
        tapGesture.enabled = YES;
        pinchGesture.enabled = YES;
        longGesture.enabled = YES;
        swipeLeftGesture.enabled = YES;
        doubleTapGesture.enabled = YES;
    }];
}



-(void)redSliderLabelTapped:(UIGestureRecognizer *)tap{
    

    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Red"
                                  message:@"Enter number between 0-255"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Number from 0-255";
        textField.keyboardType = UIKeyboardTypeNumberPad;
        
    }];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   redSlider.value = [alert.textFields[0].text floatValue];
                                                   redSliderValue = [alert.textFields[0].text floatValue];
                                                   [colorClass displayColorView:self.editableSliderViewColor withColor:[UIColor colorWithRed:redSlider.value / 255.0f green:greenSlider.value / 255.0f blue:blueSlider.value / 255.0f alpha:1.0]];
                                                   
                                                   
                                               }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    


    [self presentViewController:alert animated:YES completion:nil];
}

-(void)greenSliderLabelTapped:(UITapGestureRecognizer *)tap{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Green"
                                  message:@"Enter number between 0-255"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Number from 0-255";
        textField.keyboardType = UIKeyboardTypeNumberPad;
        
    }];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   greenSlider.value = [alert.textFields[0].text floatValue];
                                                   greenSliderValue = [alert.textFields[0].text floatValue];
                                                   //                                                   [self setRandomColorForView:self.editableSliderViewColor];
                                                   [colorClass displayColorView:self.editableSliderViewColor withColor:[UIColor colorWithRed:redSlider.value / 255.0f green:greenSlider.value / 255.0f blue:blueSlider.value / 255.0f alpha:1.0]];
                                                   
                                                   
                                               }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

-(void)blueSliderLabelTapped:(UITapGestureRecognizer *)tap{
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Blue"
                                  message:@"Enter number between 0-255"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Number from 0-255";
        textField.keyboardType = UIKeyboardTypeNumberPad;
        
    }];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   blueSlider.value = [alert.textFields[0].text floatValue];
                                                   blueSliderValue = [alert.textFields[0].text floatValue];
                                                   //                                                   [self setRandomColorForView:self.editableSliderViewColor];
                                                   [colorClass displayColorView:self.editableSliderViewColor withColor:[UIColor colorWithRed:redSlider.value / 255.0f green:greenSlider.value / 255.0f blue:blueSlider.value / 255.0f alpha:1.0]];
                                                   
                                                   
                                               }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}


#pragma mark - Convenience

- (UHColor  *)findViewUsingPoint:(CGPoint)point
{
    for (UHColor *colorView  in self.colorViews) {
        if (CGRectContainsPoint(colorView.frame, point)) {
            return colorView;
        }
    }
    return nil;
}



-(void) screenshot
{
    
    UIView *wholeScreen = self.view;
    
    // define the size and grab a UIImage from it
    UIGraphicsBeginImageContextWithOptions(wholeScreen.bounds.size, wholeScreen.opaque, 0.0);
    [wholeScreen.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *screengrab = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImageWriteToSavedPhotosAlbum(screengrab, nil, nil, nil);
}




-(BOOL)prefersStatusBarHidden{
    return YES;
}

-(void)enableAllGestures{
    tapGesture.enabled = YES;
    swipeUpGesture.enabled = YES;
    doubleTapGesture.enabled = YES;
    
    
}
-(void)disableAllGestures{
    
}

@end
