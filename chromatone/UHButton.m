//
//  UHButtonSetup.m
//  CoolorsClone
//
//  Created by Umar on 10/23/15.
//  Copyright © 2015 Umar. All rights reserved.
//

#import "UHButton.h"

@implementation UHButton

-(void)setUpButton:(UIButton *)button{
    [button setTintColor:[UIColor whiteColor]];
    [button.layer setShadowColor:[UIColor blackColor].CGColor];
    [button.layer setShadowOffset:CGSizeMake(2, 2)];
    [button.layer setShadowOpacity:.4];
    
    button.alpha = 0.0;

}

-(void)showHideButtons:(UIButton *)button andAddToSubview:(UIView *)view{
    if (!(button.alpha == 0.0f)) {
        button.alpha = 0.0f;
    }else{
        [view bringSubviewToFront:button];
        button.alpha = 1.0f;
    }
}

-(void)hideButton:(UIButton *)button{
    button.alpha = 0.0f;
}
@end
