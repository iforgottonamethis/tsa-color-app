//
//  UHColor.h
//  CoolorsClone
//
//  Created by Umar on 10/3/15.
//  Copyright © 2015 Umar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UHColor : UIView

@property NSMutableArray *items;


@property (nonatomic)NSString *redColor;
@property (nonatomic)NSString *greenColor;
@property (nonatomic)NSString *blueColor;
//@property (nonatomic)NSString *hueColor;
//@property (nonatomic)NSString *saturationColor;
//@property (nonatomic)NSString *brightnessColor;
@property (nonatomic)NSString *colorName;
@property (nonatomic)NSString *hexString;

- (void)getRandomColorAndHexForColorView:(UHColor  *)view;
-(void)setRandomColorForView:(UHColor *)view;
- (void)displayColorView:(UHColor  *)view withColor:(UIColor *)color;

@end

