//
//  UHButtonSetup.h
//  CoolorsClone
//
//  Created by Umar on 10/23/15.
//  Copyright © 2015 Umar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface UHButton : UIViewController

-(void)setUpButton:(UIButton *)button;
-(void)showHideButtons:(UIButton *)button andAddToSubview:(UIView *)view;
-(void)hideButton:(UIButton *)button;



@end
